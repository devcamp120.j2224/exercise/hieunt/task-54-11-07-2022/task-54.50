package com.task54_50;

public class Region {
    String regionCode;
    String regionName;
    //method khởi tạo không tham số
    public Region() { }
    // method khởi tạo có đủ 2 tham số
    public Region(String regionCode, String regionName) {
        this.regionCode = regionCode;
        this.regionName = regionName;
    }
    @Override
    public String toString() {
        return "{regionCode: \"" + regionCode +
            "\", regionName: \"" + regionName + "\"}";
    }
}
