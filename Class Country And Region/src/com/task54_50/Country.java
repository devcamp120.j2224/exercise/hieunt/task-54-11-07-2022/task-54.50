package com.task54_50;

import java.util.ArrayList;

public class Country {
    private String countryCode;
    private String countryName;
    private ArrayList<Region> regions;
    //method khởi tạo không tham số
    public Country() { }
    // method khởi tạo có đủ 3 tham số
    public Country(String countryCode,
        String countryName, ArrayList<Region> regions) {
        this.setCountryCode(countryCode);
        this.countryName = countryName;
        this.setRegions(regions);
    }
    // các method getter setter cho các thuộc tính của class Country
    public String getCountryCode() {
        return countryCode;
    }
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
    public ArrayList<Region> getRegions() {
        return regions;
    }
    public void setRegions(ArrayList<Region> regions) {
        this.regions = regions;
    }
    @Override
    public String toString() {
        return "{countryCode: \"" + getCountryCode() +
            "\", countryName: \"" + countryName +
            "\", regions: " + getRegions() + "}";
    }
}
