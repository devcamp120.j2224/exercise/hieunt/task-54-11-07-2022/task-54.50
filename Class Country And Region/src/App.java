import java.util.ArrayList;
import com.task54_50.*;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Country> countryList = new ArrayList<Country>();
        Country country1 = new Country("VN", "Viet Nam", new ArrayList<Region>());
        Country country2 = new Country("USA", "America", new ArrayList<Region>());
        Country country3 = new Country("AUS", "Australia", new ArrayList<Region>());
        countryList.add(country1);
        countryList.add(country2);
        countryList.add(country3);

        ArrayList<Region> regionListVN = new ArrayList<Region>();
        Region regionVN1 = new Region("HN", "Ha Noi");
        Region regionVN2 = new Region("DN", "Da Nang");
        Region regionVN3 = new Region("HCM", "Ho Chi Minh");
        regionListVN.add(regionVN1);
        regionListVN.add(regionVN2);
        regionListVN.add(regionVN3);
        country1.setRegions(regionListVN);

        System.out.println("Danh sách các country:");
        for (Country country : countryList) {
            System.out.println(country);
        }
        for (Country country : countryList) {
            if (country.getCountryCode() == "VN") {
                System.out.println("Danh sách các region của country VN");
                for (Region regionVN: country.getRegions()){
                    System.out.println(regionVN);
                }
            }
        }
    }
}
